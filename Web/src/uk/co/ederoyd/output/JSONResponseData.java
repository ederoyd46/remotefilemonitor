package uk.co.ederoyd.output;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import uk.co.ederoyd.GlobalConstants;
import uk.co.ederoyd.util.Utils;

public class JSONResponseData implements ResponseData {

	/** Content Type */
	private static final String CONTENT_TYPE = "application/json; charset=utf-8";
	
	/** File System Path */
	private String fileSystemPath;

	/**
	 * Initialise instance variables.
	 */
	public void init(String fileSystemPath, ServletContext context) {
		this.fileSystemPath = fileSystemPath;
	}

	public void handleResponse(String virtualPath, HttpServletResponse response)
			throws IOException {
		response.setContentType(CONTENT_TYPE);

		PrintWriter out = response.getWriter();
		//Set the content type of the response, this could be text or binary.
		try {
			//Write out the results of the retrieve to the writer.
			out.print(retrieve(virtualPath));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//Flush the writer (this should be done automatically, but we can do it manually to be safe).
		out.flush();
		
		//Close the stream, to release the resource.
		out.close();
	}
	
	/**
	 * Implementation of retrieve, we put the results into HTML format.
	 * @throws UnsupportedEncodingException 
	 */
	private String retrieve(String virtualPath) throws JSONException, UnsupportedEncodingException {
		//Retrieve the list of files from the path
		File[] files = Utils.retrieveListing(fileSystemPath + "/" + virtualPath);

		JSONObject master = new JSONObject();
		
		if (files != null) {
			//Loop through the list of results and build the link to the next directory down.
			for (File file : files) {
				if (file.isFile()) {
					String fileName = file.getName();
					String extension = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length()); 
					//If it's a file we want a binary link so the file can be served.
					JSONObject directory = new JSONObject();
					directory.put("type", extension); 
					directory.put("name", fileName);
					String binaryLink = buildBinaryLink(virtualPath, file.getName());
					directory.put("path", binaryLink);
					directory.put("binarypath", binaryLink);
					master.put(directory.getString("name"), directory);
				} else {
					//If it's a directory we want to just display links to it's children.
					JSONObject directory = new JSONObject();
					directory.put("type", "directory");
					directory.put("name", file.getName());
					directory.put("path", buildDirectoryLink(virtualPath, file.getName()));
					directory.put("binarypath", buildBinaryLink(virtualPath, file.getName()));
					master.put(directory.getString("name"), directory);
				}
				
			}
		}

		//Build the String and return the result to the caller.
		return master.toString();
	}
	
	private String buildLink(Type type, String currentVirtualPath, String childPath) throws UnsupportedEncodingException {
		//In case we're at the root of the path, we don't need the currentVirtualPath in the link as it is blank.
		if (currentVirtualPath == null || currentVirtualPath == "") {
			return MessageFormat.format("type={0}&path={1}", type, 
					URLEncoder.encode(childPath, GlobalConstants.URL_ENCODING));
		}
		
		//For navigating the tree below the root, so we need the currentVirtualPath
		return MessageFormat.format("type={0}&path={1}/{2}", type, 
				URLEncoder.encode(currentVirtualPath, GlobalConstants.URL_ENCODING), 
				URLEncoder.encode(childPath, GlobalConstants.URL_ENCODING));
	}
	
	private String buildDirectoryLink(String currentVirtualPath, String childPath) throws UnsupportedEncodingException {
		return buildLink(Type.JSON, currentVirtualPath, childPath);
	}
	
	private String buildBinaryLink(String currentVirtualPath, String childPath) throws UnsupportedEncodingException {
		return buildLink(Type.BINARY, currentVirtualPath, childPath);
	}
	
	

}
