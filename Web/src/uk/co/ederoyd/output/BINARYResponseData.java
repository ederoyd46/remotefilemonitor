package uk.co.ederoyd.output;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import uk.co.ederoyd.util.Utils;

public class BINARYResponseData implements ResponseData {
	
	private static final int BUFFER_SIZE = 4 * 1024; //4K Buffer 

	/** File System Path */
	private String fileSystemPath;
	
	/** Servlet Context */
	private ServletContext context;

	public void init(String fileSystemPath, ServletContext context) {
		this.fileSystemPath = fileSystemPath;
		this.context = context;
	}
	
	public void handleResponse(String virtualPath, HttpServletResponse response) throws IOException {
		//Retrieve the list of files from the path
		File file = Utils.retrieveFile(fileSystemPath + "/" + virtualPath);
		
		if (file == null) return;
		
		if (file.isDirectory()) {
			//here we want to zip up the directory first.
			file = zip(file);
		}
		
		InputStream is = new FileInputStream(file);
		ServletOutputStream out = response.getOutputStream();

		//Set the response headers, these tell the browser how to handle the data.
		response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

		//Set the content type of the response, this could be text or binary.
		response.setContentType(context.getMimeType(file.getAbsolutePath()));
		
		//Set the length so the browser can show the download progress.
		response.setContentLength(is.available());
		
		byte[] buf = new byte[BUFFER_SIZE];
		int bytesRead;
		
		try {
			while ((bytesRead = is.read(buf)) != -1) {
				//Write out the results of the retrieve to the writer.
				out.write(buf, 0, bytesRead);
			}
		} finally {
			if (is != null)
				is.close();
		}
		
		//Flush the writer (this should be done automatically, but we can do it manually to be safe).
		out.flush();
		
		//Close the stream, to release the resource.
		out.close();
	}
	
	/**
	 * TODO Change this to create a temporary file.
	 * @param location
	 * @return
	 * @throws IOException
	 */
	private File zip(File location) throws IOException {
		//We create the temporary zip file that will be served to the user. 
		String tempFileName = "/tmp/" + location.getName() + ".zip";
		
		//Output stream this is the object that writes the data out.
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(tempFileName));
		
		//Buffer size is 4k
		byte[] buf = new byte[BUFFER_SIZE];
		
		//This gets us a list a files underneath the directory
		File[] files = location.listFiles();
		
		//For each file we want to add to the zip file.
		for (File file : files) {
			//At the moment we can only zip files, so check we're not trying to zip a directory.
			if (file.isDirectory()) continue;
			
			//Create new input stream, this reads the data.
			FileInputStream is = new FileInputStream(file);

			//Create new entry to add to the zip
			ZipEntry entry = new ZipEntry(file.getName());
			out.putNextEntry(entry);

			int bytesRead;
			while ((bytesRead = is.read(buf)) != -1) {
				//Write the data in chunks of 4k to the file.
				out.write(buf, 0, bytesRead);
			}
			//finally make sure we close the file so we release the OS lock.
			is.close();
		}
		
		//Close the zip file so we can read it back in a minute.
		out.close();
		return new File(tempFileName) ;
	}
     
}
