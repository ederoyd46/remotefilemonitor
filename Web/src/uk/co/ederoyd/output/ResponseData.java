package uk.co.ederoyd.output;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

public interface ResponseData {

	void init(String fileSystemPath, ServletContext context);
	void handleResponse(String virtualPath, HttpServletResponse response) throws IOException;
}
