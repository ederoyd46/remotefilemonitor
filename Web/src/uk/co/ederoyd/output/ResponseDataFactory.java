package uk.co.ederoyd.output;

public class ResponseDataFactory {

	public static ResponseData createResponse(Type type) throws InvalidResponseDataType {
		if (Type.HTML == type) {
			return new HTMLResponseData();
		}
		
		if (Type.BINARY == type) {
			return new BINARYResponseData();
		}
		
		if (Type.JSON == type) {
			return new JSONResponseData();
		}
		throw new InvalidResponseDataType("The type " + type + " has not been implemented yet");
	}
	
}
