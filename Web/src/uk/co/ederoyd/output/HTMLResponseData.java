package uk.co.ederoyd.output;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import uk.co.ederoyd.GlobalConstants;
import uk.co.ederoyd.util.Utils;

public class HTMLResponseData implements ResponseData {

	/** Content Type */
	private static final String CONTENT_TYPE = "text/html; charset=utf-8";

	/** HTML Header Constant */
	private static final String HTML_HEADER = "<html><head /><body>";

	/** HTML Footer Constant */
	private static final String HTML_FOOTER = "</body></html>";

	/** HTML Line Break */
	private static final String HTML_LINE_BREAK = "<br />";
	
	/** File System Path */
	private String fileSystemPath;
	
	/** Servlet Context */
	private ServletContext context;
	
	/**
	 * Initialise instance variables.
	 */
	public void init(String fileSystemPath, ServletContext context) {
		this.fileSystemPath = fileSystemPath;
		this.context = context;
	}
	
	public void handleResponse(String virtualPath, HttpServletResponse response) throws IOException {
		response.setContentType(CONTENT_TYPE);

		PrintWriter out = response.getWriter();
		//Set the content type of the response, this could be text or binary.
		
		//Write out the results of the retrieve to the writer.
		out.print(retrieve(virtualPath));
		
		//Flush the writer (this should be done automatically, but we can do it manually to be safe).
		out.flush();
		
		//Close the stream, to release the resource.
		out.close();
	}

	/**
	 * Implementation of retrieve, we put the results into HTML format.
	 * @throws UnsupportedEncodingException 
	 */
	private String retrieve(String virtualPath) throws UnsupportedEncodingException {
		//Create a new StringBuilder class
		StringBuilder builder = new StringBuilder();

		//Add the HTML_HEADER to the beginning of the response.
		builder.append(HTML_HEADER);
		
		//Retrieve the list of files from the path
		File[] files = Utils.retrieveListing(fileSystemPath + "/" + virtualPath);
		if (files != null) {
			//Loop through the list of results and build the link to the next directory down.
			for (File file : files) {
				if (file.isFile()) {
					//If it's a file we want a binary link so the file can be served.
					builder.append(buildBinaryLink(virtualPath, file.getName()));
				} else {
					//If it's a directory we want to just display links to it's children.
					builder.append(buildDirectoryLink(virtualPath, file.getName()));
				}
				
				builder.append(HTML_LINE_BREAK);
			}
		}

		//Add the HTML_HEADER to the beginning of the response.
		builder.append(HTML_FOOTER);
		
		//Build the String and return the result to the caller.
		return builder.toString();
	}
	
	/**
	 * Build a hyperlink for the children of the current virtual path.
	 * @param currentVirtualPath Current virtual path
	 * @param childPath Child path.
	 * @return hyper link for the child.
	 * @throws UnsupportedEncodingException 
	 */
	private String buildLink(Type type, String currentVirtualPath, String childPath) throws UnsupportedEncodingException {
		//In case we're at the root of the path, we don't need the currentVirtualPath in the link as it is blank.
		if (currentVirtualPath == null || currentVirtualPath == "") {
			return MessageFormat.format("<a href=\"{0}/monitor?type={1}&path={2}\">{3}</a>", 
					context.getContextPath(), type, 
					URLEncoder.encode(childPath, GlobalConstants.URL_ENCODING),
					childPath);
		}
		
		//For navigating the tree below the root, so we need the currentVirtualPath
		return MessageFormat.format("<a href=\"{0}/monitor?type={1}&path={2}/{3}\">{4}</a>", 
				context.getContextPath(), type, 
				URLEncoder.encode(currentVirtualPath, GlobalConstants.URL_ENCODING), 
				URLEncoder.encode(childPath, GlobalConstants.URL_ENCODING),
				childPath);
	}
	
	private String buildDirectoryLink(String currentVirtualPath, String childPath) throws UnsupportedEncodingException {
		return buildLink(Type.HTML, currentVirtualPath, childPath);
	}
	
	private String buildBinaryLink(String currentVirtualPath, String childPath) throws UnsupportedEncodingException {
		return buildLink(Type.BINARY, currentVirtualPath, childPath);
	}
}
