package uk.co.ederoyd.output;

public enum Type {
	HTML,
	JSON,
	BINARY
}
