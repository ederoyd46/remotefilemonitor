package uk.co.ederoyd.output;

public class InvalidResponseDataType extends Exception {

	private static final long serialVersionUID = -4240268084684094100L;

	public InvalidResponseDataType(String message) {
		super(message);
	}
}
