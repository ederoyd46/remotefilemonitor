package uk.co.ederoyd.util;

import java.io.File;

public class Utils {

	
	/**
	 * Private constructor so we can't instantiate the class.
	 */
	private Utils() {
		//Nothing
	}
		
	public static File[] retrieveListing(String path) {
		File location = new File(path);
		return location.listFiles();
	}
	
	public static File retrieveFile(String path) {
		File file = new File(path);
		return file; 
	}
}
