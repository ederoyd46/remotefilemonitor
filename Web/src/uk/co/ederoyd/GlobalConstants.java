package uk.co.ederoyd;

public class GlobalConstants {

	/**
	 * Private so we can't instantiate the class.
	 */
	private GlobalConstants() {
		//Nothing
	}
	
	/** Init Parameter File Path */
	public static final String INIT_PARAMETER_FILE_PATH = "filePath";

	/** Path Parameter */
	public static final String PARAMETER_PATH = "path";
	
	/** Type Parameter */
	public static final String PARAMETER_TYPE = "type";

	/** Encoding used for URL's. */
	public static final String URL_ENCODING = "UTF-8";


	
}
