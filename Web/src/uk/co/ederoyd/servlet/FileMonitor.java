package uk.co.ederoyd.servlet;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uk.co.ederoyd.GlobalConstants;
import uk.co.ederoyd.output.InvalidResponseDataType;
import uk.co.ederoyd.output.ResponseData;
import uk.co.ederoyd.output.ResponseDataFactory;
import uk.co.ederoyd.output.Type;

public class FileMonitor extends HttpServlet {

	private static final long serialVersionUID = 5948819662975509146L;
	
	/** File Path */
	private String filePath;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//If we get a POST request, treat it as a GET request.
		doGet(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//Set the encoding here so we can decode parameters with umlauts in.
		request.setCharacterEncoding(GlobalConstants.URL_ENCODING);
		
		//Retrieve the parameters from the URL.
		String pPath = request.getParameter(GlobalConstants.PARAMETER_PATH);
		String pType = request.getParameter(GlobalConstants.PARAMETER_TYPE);
		
		//Some basic checks to set the defaults in case they are not populated.
		if (pPath == null) pPath = "";
		if (pType == null) pType = Type.HTML.toString();
		String encoding = request.getCharacterEncoding();
		pPath = URLDecoder.decode(pPath, GlobalConstants.URL_ENCODING);

		
		//Validate the type of response we want.
		Type type = Type.valueOf(pType); //TODO Add an exception here, this could fail.
		
		ResponseData responseData;
		try {
			//Call the factory method, this creates the instance of the response we want.
			//We just get the interface as we don't care about the implementation here.
			responseData = ResponseDataFactory.createResponse(type);
		} catch (InvalidResponseDataType e) {
			throw new ServletException(e);
		}
		
		//Initialise the responseData, here we can set up any data we want.
		responseData.init(filePath, getServletContext());
		responseData.handleResponse(pPath, response);
	}
		
	@Override
	public void init() throws ServletException {
		this.filePath = getInitParameter(GlobalConstants.INIT_PARAMETER_FILE_PATH);
		super.init();
		
		
		
		
	}
	


}
