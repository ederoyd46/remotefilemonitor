package uk.co.ederoyd;

public class GlobalConstants {

	public static final String APPLICATION_NAME = "RemoteFileClient";

	public static final String SERVER_ADDRESS = "http://10.0.1.3:8080/RemoteFileMonitor/monitor?";
	
	private GlobalConstants() {
		
	}
}
