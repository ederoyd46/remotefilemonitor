package uk.co.ederoyd.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import uk.co.ederoyd.GlobalConstants;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class Utils {

	private Utils() {
		
	}
	
	
	public static HttpClient createHttpClient() { 
        Log.d(GlobalConstants.APPLICATION_NAME,"createHttpClient()..."); 
        HttpParams params = new BasicHttpParams(); 
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1); 
        HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET); 
        HttpProtocolParams.setUseExpectContinue(params, true); 
    
        SchemeRegistry schemeRegistry = new SchemeRegistry(); 
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80)); 
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443)); 
        ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params,schemeRegistry); 
        return new DefaultHttpClient(conMgr, params); 
    } 
	
	public static void shutdownHttpClient(HttpClient httpClient) { 
        if(httpClient!=null && httpClient.getConnectionManager()!=null) { 
            httpClient.getConnectionManager().shutdown(); 
        } 
    } 

	public static JSONObject executeJSONHttpGet(HttpClient client, String url) throws Exception {		
		HttpResponse response = executeHttpGet(client, url);
        BufferedReader in = null; 
		try {		
	        in = new BufferedReader(new InputStreamReader(
	        		response.getEntity().getContent()));
	        StringBuilder sb = new StringBuilder(); 
	        String line = ""; 
	        while ((line = in.readLine()) != null) { 
	            sb.append(line); 
	        } 
	        in.close(); 
	        String result = sb.toString(); 
	        return new JSONObject(result); 
	    } finally { 
	        if (in != null) { 
	            try { 
	                in.close(); 
	            } catch (IOException e) { 
	                e.printStackTrace(); 
	            } 
	        } 
	    } 
	}
	
	public static String executeBINARYHttpGet(HttpClient client, String url, String location) throws Exception {		
		HttpResponse response = executeHttpGet(client, url);
		//InputStream is = response.getEntity().getContent();

		Header header = response.getFirstHeader("Content-Disposition");

		if (header == null) {
			//TODO Throw exception 
		}
		
		//TODO There must be a cleaner way to do this.
		String fileName = header.getValue();
		fileName = fileName.replaceAll("attachment; filename=", "");
		fileName = fileName.replaceAll("\"", "");
		String localFileName = location + "/" + fileName;
		FileOutputStream out = new FileOutputStream(new File(localFileName));
		try {
			InputStream in = response.getEntity().getContent();
			//response.getEntity().writeTo(out);
			byte[] buf = new byte[4 * 1024];  // 4K buffer
			int bytesRead;
			try {
				while ((bytesRead = in.read(buf)) != -1) {
					//Write out the results of the retrieve to the writer.
					out.write(buf, 0, bytesRead);
				}
			} finally {
				if (in != null)
					in.close();
				out.flush();
				out.close();
			}
		} finally {
			if (out != null) {
				out.close();
			}
		}
		return localFileName;
		
	}
	
	public static HttpResponse executeHttpGet(HttpClient client, String url) throws Exception { 
        HttpGet request = new HttpGet(); 
        request.setURI(new URI(url)); 
        HttpResponse response = client.execute(request); 
        return response;
	}
	
	
    public static List<String> handleZipFile(String fileName) throws IOException {
    	String directoryName = fileName.substring(fileName.lastIndexOf("/") +1, fileName.indexOf(".zip"));  
    	String location = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ederoyd/" + directoryName;

		//we are a binary file
		File pathCheck = new File(location);
		if (!pathCheck.exists()) {
			pathCheck.mkdirs();
		}
    	
		File file = new File(fileName);
    	ZipFile zipFile = new ZipFile(file);
    	List<String> localFileNames = new ArrayList<String>();
    	
    	Enumeration<? extends ZipEntry> zipEntries = zipFile.entries();
    	
    	while (zipEntries.hasMoreElements()) {
    		ZipEntry entry = zipEntries.nextElement();
    		InputStream in = zipFile.getInputStream(entry);
    		String localFileName = location + "/" + entry.getName();
    		localFileNames.add(localFileName);
    		
    		OutputStream out = new FileOutputStream(new File(localFileName));
    		byte[] buf = new byte[4 * 1024];  // 4K buffer
    		int bytesRead;
    		try {
    			while ((bytesRead = in.read(buf)) != -1) {
    				//Write out the results of the retrieve to the writer.
    				out.write(buf, 0, bytesRead);
    			}
    		} finally {
    			if (in != null) in.close();
    			if (out != null) out.close();
    		}
    	}
    	
    	zipFile.close();
    	try {
    		file.delete();
    	} catch (Exception e) {
    		Log.e(GlobalConstants.APPLICATION_NAME, e.getMessage());
    	}
    	
    	return localFileNames;
    }	
	
	
	public static void addToMediaStore(Context context, String fileName) {
		ArrayList<String> fileNames = new ArrayList<String>();
		fileNames.add(fileName);
		addToMediaStore(context, fileNames);
	}
	
	public static void addToMediaStore(Context context, List<String> fileNames) {
		new StoreMedia(context, fileNames);
	}
	
}
