package uk.co.ederoyd.util;

import java.util.List;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;

public class StoreMedia implements MediaScannerConnectionClient {

	private List<String> fileNames;
	private MediaScannerConnection mediaScanner;
	
	public StoreMedia(Context context, List<String> fileNames) {
		this.fileNames = fileNames;
		mediaScanner = new MediaScannerConnection(context, this);
		mediaScanner.connect();
	}
	
	public void onMediaScannerConnected() {
		for (String fileName : fileNames) {
			mediaScanner.scanFile(fileName, null);
		}
	}

	public void onScanCompleted(String path, Uri uri) {
		// TODO Auto-generated method stub
		
	}
	
	
}


