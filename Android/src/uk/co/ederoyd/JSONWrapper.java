package uk.co.ederoyd;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JSONWrapper implements Comparable<JSONWrapper>{

	private String key;
	private JSONObject jsonItem;
	
	
	public JSONWrapper(String key, JSONObject jsonItem) {
		this.key = key;
		this.jsonItem = jsonItem;
	}
	

	public int compareTo(JSONWrapper another) {
		return getName().compareTo(another.getName());
	}


	public String getKey() {
		return key;
	}


	public String getType() {
		try {
			return jsonItem.getString("type");	
		} catch (JSONException e) {
			Log.e(GlobalConstants.APPLICATION_NAME, "An error occurred while trying to jsonItem.getString(\"type\")");
		}
		
		return "";
	}


	public String getName() {
		try {
			return jsonItem.getString("name");	
		} catch (JSONException e) {
			Log.e(GlobalConstants.APPLICATION_NAME, "An error occurred while trying to jsonItem.getString(\"type\")");
		}
		
		return "";
	}


	public String getPath() {
		try {
			return jsonItem.getString("path");	
		} catch (JSONException e) {
			Log.e(GlobalConstants.APPLICATION_NAME, "An error occurred while trying to jsonItem.getString(\"path\")");
		}
		
		return "";
	}


	public String getBinaryPath() {
		try {
			return jsonItem.getString("binarypath");	
		} catch (JSONException e) {
			Log.e(GlobalConstants.APPLICATION_NAME, "An error occurred while trying to jsonItem.getString(\"binarypath\")");
		}
		
		return null;
	}

}
