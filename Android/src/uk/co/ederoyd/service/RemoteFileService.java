package uk.co.ederoyd.service;

import java.io.File;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.http.client.HttpClient;

import uk.co.ederoyd.GlobalConstants;
import uk.co.ederoyd.Main;
import uk.co.ederoyd.R;
import uk.co.ederoyd.util.Utils;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

public class RemoteFileService extends Service {

    private HttpClient httpClient; 
    private NotificationManager notificationManager; 
    private LinkedBlockingQueue<HashMap<String, String>> queue = null;
    private Thread workerThread = null;
    
    
    
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override 
    public void onCreate() { 
        super.onCreate(); 
        httpClient = Utils.createHttpClient();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        queue = new LinkedBlockingQueue<HashMap<String, String>>(20);
        workerThread = new Thread(null, new ServiceWorker(), GlobalConstants.APPLICATION_NAME); 
        workerThread.start();     
    } 
	
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		String path = intent.getStringExtra("path");
		String name = intent.getStringExtra("name");

		if (path == null) {
			return;
		}
		
		HashMap<String, String> entry = new HashMap<String, String>();
		entry.put("path", path);
		entry.put("name", name);
		sendNotification(name, "Queued...");
		queue.offer(entry);

	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (workerThread != null) workerThread.stop();
		queue.clear();
        Utils.shutdownHttpClient(httpClient);
	}

	private void sendNotification(String title, String message) { 
        Notification notification = new Notification(
        		R.drawable.icon, message + " " + title, System.currentTimeMillis()); 
        
        PendingIntent contentIntent = PendingIntent.getActivity(
        		this, 0, new Intent(this, Main.class), Intent.FLAG_ACTIVITY_NEW_TASK); 

        notification.setLatestEventInfo(this, title, message, contentIntent); 
        
        notificationManager.notify(R.string.app_name, notification); 
    } 

    private void handleFile(String path) throws Exception {
		String location = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ederoyd";
		//we are a binary file
		File pathCheck = new File(location);
		if (!pathCheck.exists()) {
			pathCheck.mkdirs();
		}
		String localFileName = Utils.executeBINARYHttpGet(httpClient, GlobalConstants.SERVER_ADDRESS + path, location);
		if (localFileName.endsWith(".zip")) {
			//we need to unzip it first
			Utils.addToMediaStore(this, Utils.handleZipFile(localFileName));
		} else {
			Utils.addToMediaStore(this, localFileName);
		}
    }
	
	class ServiceWorker implements Runnable {
		public void run() {
			while (true) {
				try {
					HashMap<String, String> entry = queue.take();
					String path = entry.get("path");
					String name = entry.get("name");

					try {
						sendNotification(name, "Downloading...");
						handleFile(path);
						sendNotification(name, "Downloaded");
					} catch (Exception e) {
						Log.e(GlobalConstants.APPLICATION_NAME, "Error occurred while downloading the file.", e);
						sendNotification(name, "Failed: " + e.getMessage());
						continue;
					}
				} catch (InterruptedException e) {
					Log.e(GlobalConstants.APPLICATION_NAME, "An error occurred on the thread", e);
				}
			}
		}
	}
}
