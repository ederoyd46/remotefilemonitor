package uk.co.ederoyd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Stack;

import org.apache.http.client.HttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.ederoyd.service.RemoteFileService;
import uk.co.ederoyd.util.Utils;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemLongClickListener;

public class Main extends ListActivity implements OnItemLongClickListener {
	
	private HttpClient client = null;
	
	private PageLocation pageLocation = null;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        client = Utils.createHttpClient();
        setContentView(R.layout.main);
        pageLocation = new PageLocation();
        getListView().setOnItemLongClickListener(this);
//      SharedPreferences preferences = getPreferences(MODE_PRIVATE);  	
//    	Editor editor = preferences.edit();
//    	editor.putString("encoding", "UTF-8");
//    	editor.commit();
    	
    	initialList();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	initialList();
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	stopService(new Intent(this, RemoteFileService.class));
    }
    
    private void initialList() {
    	try {
    		JSONObject json = Utils.executeJSONHttpGet(client, GlobalConstants.SERVER_ADDRESS + "type=JSON");
    		setJSONasList(json);
    		pageLocation.reset();
    		pageLocation.forward(json);
    	} catch (Exception e) {
    		Log.e(GlobalConstants.APPLICATION_NAME, e.getMessage());
    	}
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	if (keyCode == KeyEvent.KEYCODE_BACK) {
    		JSONObject json = pageLocation.backwards();
    		if (json != null) {
    			try {
        			setJSONasList(json);
        			return true;
    			} catch (Exception e) {
    				//Ignore and continue as if there was no JSON.
    			}
    		}
    	}
    	
    	return super.onKeyDown(keyCode, event);
    }
    
    @SuppressWarnings("unchecked")
	private void setJSONasList(JSONObject json) throws Exception {
		ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();
		
		List<JSONWrapper> jsonWrappedlist = wrapJSONObject(json); 
		Collections.sort(jsonWrappedlist);
		
		for (JSONWrapper jsonItem : jsonWrappedlist) {
    		LinkedHashMap<String,String> item = new LinkedHashMap<String,String>();
    		item.put("key", jsonItem.getKey());
    		item.put("type", jsonItem.getType());
    		item.put("name", jsonItem.getName());
    		item.put("path", jsonItem.getPath());
    		item.put("binarypath", jsonItem.getBinaryPath());
    		list.add(item);
		}
		
		SimpleAdapter adapter = new SimpleAdapter( 
				this, 
				list,
				R.layout.main,
				new String[] { "key" },
				new int[] { R.id.text1 } );
		
		
		setListAdapter(adapter);
    }
    
    @SuppressWarnings("unchecked")
	private List wrapJSONObject(JSONObject json) throws JSONException {
		Iterator<String> itr = json.keys();
		List<JSONWrapper> list = new ArrayList<JSONWrapper>();
		while (itr.hasNext()) {
			String key = itr.next();
			JSONObject jsonItem = json.getJSONObject(key);
			list.add(new JSONWrapper(key, jsonItem));
		}    	

    	return list;
    }
    
    
    
    @SuppressWarnings("unchecked")
	public void onListItemClick(ListView l, View v, int position, long id) {
    	HashMap<String, String> map = (HashMap<String, String>) l.getItemAtPosition(position);
    	
    	if ("directory".equals(map.get("type"))) {
    		handleDirectory(map.get("path"));
    	} else {
    		handleFile(map.get("path"), map.get("name"));
    	}
    }
    
	@SuppressWarnings("unchecked")
	public boolean onItemLongClick(AdapterView<?> l, View v, int position, long id) {
    	HashMap<String, String> map = (HashMap<String, String>) l.getItemAtPosition(position);
    	handleFile(map.get("binarypath"), map.get("name"));
    	return true;
	}

    private void handleDirectory(String path) {
    	try {
    		JSONObject json = Utils.executeJSONHttpGet(client, GlobalConstants.SERVER_ADDRESS + path);
    		setJSONasList(json);
    		pageLocation.forward(json);
    	} catch (Exception e) {
    		Log.e(GlobalConstants.APPLICATION_NAME, e.getMessage());
    	}
    }
    
    private void handleFile(String path, String name) {
    	Intent intent = new Intent(this, RemoteFileService.class);
    	intent.putExtra("path", path);
    	intent.putExtra("name", name);
        startService(intent);
    }
    
    private static class PageLocation {
    	private Stack<JSONObject> jsonStack = null;
    	private JSONObject currentJSON = null;
    	
    	
    	public PageLocation() {
			jsonStack = new Stack<JSONObject>();
		}
    	
    	public void forward(JSONObject json) {
    		if (currentJSON != null) {
    			jsonStack.push(currentJSON);
    		}
    		currentJSON = json;
    	}
    	
    	public JSONObject backwards() {
    		if (jsonStack.size() > 0) {
    			currentJSON = jsonStack.pop();
    			return currentJSON; 
    		}
    		
    		return null;
    	}
    	
    	public void reset() {
    		jsonStack.clear();
    		currentJSON = null;
    	}
    	
    }
}